import java.util.Scanner;
import java.util.Random;
public class Wordle {




    public static void runGame(String word) {


        System.out.println("What is your guess?");
        int attempts = 5;
        String guess = readGuess();

        while (attempts > 0) {
            presentResults(guess, guessWord(word, guess));

            if (guess.equals(word)) {
                System.out.println("Congratulations, you've got the word correct and have won this round of the wordle game!!!");
                break;
            } else {
                System.out.println("Try again");
                System.out.println("You Have " + attempts + " attempts remaining");
                attempts = attempts - 1;
                guess = readGuess();

            }
        }
    }


    public static String[] guessWord(String answer, String guess) {


        String [] colorfulArray = new String[5];


        for (int i = 0; i < guess.length(); i++) {
            char letter = guess.charAt(i);
            if (letterInSlot(answer, letter, i)) {

                colorfulArray[i] = "green";
            } else if (letterInWord(answer, letter)) {

                colorfulArray[i] = "yellow";
            } else {

                colorfulArray[i] = "white";
            }
        }
        return colorfulArray;
    }

    public static String presentResults(String guess, String[] colorfulArray) {

        String coloredLetter = guess;

        for (int i = 0; i < guess.length(); i++) {
            char guessLetter = coloredLetter.charAt(i);
            if (colorfulArray[i] == "green") {
                System.out.println("\u001B[32m" + guessLetter + "\u001B[0m");

            } else if (colorfulArray[i] == "yellow") {
                System.out.println("\u001B[33m" + guessLetter + "\u001B[0m");
            } else if (colorfulArray[i] == "white") {
                System.out.println("\u001B[0m" + guessLetter);

            }

        }

        return coloredLetter;
    }

    public static String readGuess() {
        Scanner reader = new Scanner(System.in);
        String guess = reader.next();

        while (true) {
            if (guess.length() != 5) {
                System.out.println("invalid guess, 5 letters words only");
                guess = reader.next();
            } else {

                break;
            }
        }
        guess = guess.toUpperCase();
        return guess;
    }




    public static String generateWord() {
        Random randomNumber = new Random();
        String[] wordList = {"JOCKY", "QUICK", "BLAZE", "DIRTY", "COMFY", "CHOMP", "DUCKS", "JUDAS", "MINOS", "JUDGE", "AZURE", "DEVIL", "ANGEL", "WORMS", "GLOCK", "BRICK", "PIGMY", "VODKA", "WHELP", "ROCKS"};
        int randomSelection = randomNumber.nextInt(wordList.length);
        String word = wordList[randomSelection];
        System.out.println(word);

        return word;

    }



    public static boolean letterInWord (String word, char letter) {
        boolean result = false;
        for (int i = 0; i < word.length(); i++) {

            char wordLetter = word.charAt(i);

            if (letter == wordLetter) {

                result = true;
                return result;
            }


        }
        return result;

    }



    public static boolean letterInSlot (String word, char letter, int position) {
        boolean result = false;
        System.out.println(letter);
        for (int i = 0; i < 1; i++) {

            char wordLetter = word.charAt(position);

            if (letter == wordLetter) {

                result = true;
                return result;
            } 

        }
        return result;

    }

}