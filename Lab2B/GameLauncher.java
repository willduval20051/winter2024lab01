import java.util.Scanner;
public class GameLauncher{
	//the main body of this class, where it initiall greet the new user of the programs and asks them to choose between playing hangman or world.
	public static void main(String[] args){
		
		Scanner reader = new Scanner(System.in);
		System.out.println("Hello and welcome to our game hub, today we have two game currently available for you to play.");
		System.out.println("Those games being Hangman or Wordle, and to start begin either of these game enter either hangman or wordle.");
		String hangmanGame = "hangman";
		String wordleGame = "wordle";
		//these two variables are used later when scanning/checking the user answer as the system will compare the player choice with one of these two variables and activate the corresponding game.
		
		String playersChoice = reader.next();
		boolean playingGame = true;		
		
		//these if statements check which choice the players made and will activate the beginning "main" or required data for the beginning of these ganaes.
		if (playersChoice.equals(hangmanGame)){
			
			while (playingGame){
			System.out.println("Please enter a 4-letter word to start the game: ");	
			String playersWord = reader.next(); 
			Hangman.runGame(playersWord);
			//calls/actiavte the hangman game.
			
				//This sections ask the user if they wish to play the hangman game again or not, with either to restarting the game if the user says yes or stopping the program if they say no.
				System.out.println("Do you wish to contine playing: (yes/no)");
				String usersResponseHangman = reader.next();
				if (usersResponseHangman.equals("yes")){
					System.out.println("ok, here your next round");            
				}
				else if (usersResponseHangman.equals("no")){
					System.out.println("Thank you for playing");
					playingGame = false;
				}
			}
		}
		else if (playersChoice.equals(wordleGame)){
			//calls/actiavte the wordle game.
			while (playingGame){
				String wordAnswer = Wordle.generateWord();
				Wordle.runGame(wordAnswer);
		
				//This sections ask the user if they wish to play the hangman game again or not, with either to restarting the game if the user says yes or stopping the program if they say no.
				System.out.println("Do you wish to contine playing: (yes/no)");
				String usersResponse = reader.next();
				if (usersResponse.equals("yes")){
					System.out.println("ok, here your next round");            
				}
				else if (usersResponse.equals("no")){
					System.out.println("Thank you for playing");
					playingGame = false;
				}
			}
		}
		
	}
	
	
	
	
	
	
}