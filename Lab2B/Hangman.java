//Name: William Duval, Student ID: 2338403, Section: 0003.
//imports/implement the scanner system that will allow for the the additional implementation of new scanner systems (in methods)that allow reader input.

import java.util.Scanner; 
//creates the public class wordGuessWork.
public class Hangman{
	

	
	//Creates a int method, called isLetterInWord, that takes in a string(word) and a char (playerAnswer/playersAnswer).
	
	
	
	
	public static int isLetterInWord(String word, char playerAnswer)
	{
		int wordPosition = 0;
		int searchResult = -1; 
		//Checks each letter of the word (with it beginning at 0 or the first letter due to the variable, position), and check if the player guess/answers is in the word.
		while (wordPosition <= 3)
		{
			if (word.charAt(wordPosition) == playerAnswer)
			{	
			//If it can find the player letter/guess in the word, it will change the searchResult variable to whatever the position variable is (0,1,2,3) and return that back to the runGame method (and also end the method with break).		
				searchResult = wordPosition;
				break;
			}
			else
			{
				//It increase until it reaches a position of three, with it check from 0-3 or letters 1-4 if the players guess is in their(with it continously looping annd increasing the position variable, thus the position/char on the word, to check).
				wordPosition += 1;
			}
		}	
		//If it can't find the player guess (or letter) in the word it will leave the searchResult variable as is and just return it back to the runGame method as is (-1).
		System.out.println(searchResult);
		//It will print out the final searchResult number before returning it back to the runGame method.
		return searchResult;
	}
	
	//Creates void method, called printWorld, that takes 4 boolean (that will either be autamitacally false, or changed (usually only one) to true if the user guesss correctly one of the words letters) and the a String (word).
	public static void printWord(String word, boolean[] lettersOfWord){	
	//Scans each of the 4 letter of the reader's inputted word.

		String resultText = "Your result is: ";	
		//It will than see if the letter the user put in is true(boolean), and if yes will print that letter the user guessed in it proper place (ex: word is "game", user guesses 'a' and will go down the lsit until reaching if statement for Letter 1 and see it true and prine that guessers letter in that position)).
		//If the letter/word the users guess is wrong in any of the if statements(letters of the word) it will instead print out "_" for that letter position of the word.
		
		for (int i = 0; i < lettersOfWord.length; i++){
		if (lettersOfWord[i]){
			resultText += word.charAt(i);
		}
		else {
			resultText += "_";
		}
		
		//It will then finally after scanning each letter position it will add on to the previous print out "Your result is" with whatever you got either that be full "_", "_a__" or the previous witha dditional word (as it save the word you got right and add/change the result.
		System.out.println(resultText);
		}
	}
	
	
	//Creates a void method, called runGame, that acts as the main frame of the system game itself and take in a String from the main method (the word the player put in).
	public static void runGame(String word){
		//Creates also a new scanner which will allow the system to save/scan the reader input.
		Scanner reader = new Scanner(System.in);
		//It set up 4 boolean variables which correspond to the letters of the word (which will be four letters long) and set them as all false (meaning that currently the user, even though they have't guesssed yet) has got their 'guess' wrong.
		boolean[] lettersOfWord = new boolean[word.length()];
		//It set up the number of attemps (which is a variable) the player will have in guessing (set to 0 for now).
		int attemps = 5;
		
		//Mainly the method set up the player guess for a word with the variables playerGuess and PlayerAnswer.
		while (attemps > 0){
			//(Read After)It will then call the the prinrWorld method and it will print out a result with either your guess in letter in it (ex: "__a_" or "b_n_") or a still blank/unchanged result.
			printWord(word, lettersOfWord);
			String playerGuess = reader.next();
			char playerAnswer = playerGuess.charAt(0);
			int result = isLetterInWord(word, playerAnswer);
			//It then goes through the loop, where after it calls the isLetterInWord method to see if the player guess of a letter is actually in the word and give back a value (0-3 position if guess correctly and -1 if guessed incorrectly), and will transform the boolean variable (Letters0-3 variables) to True.
			
			if (result >= 0 && result <= 3){
				lettersOfWord[result] = true;
			}
			else
			{
				attemps -= 1;
			}
			//It final action before looping will check if you got the whole word right (printing out that you win the game) or if you didn't get the word right or haven't fully completed the word (ex: "b____" and etc.) will print out how many time you got it wrong.
			//Finally it will continoulsy loop and perform the actions above until either the player get the word right or get 5 guesses wrong (the wrongGuesses) or the player one printing out "You have won the game".
			if (lettersOfWord[0] && lettersOfWord[1] && lettersOfWord[2] && lettersOfWord[3])
			{
				System.out.println("Congratualtions, You've have won the game!");
				break;
			}
			else
			{
				System.out.println("You've got " + attemps + " attemps left");
			}
		}
	}
}
	
	
	//Reflection1: One of the change I would have made, if I knew properly how to do, is instead of asking the reader to type in the word they will be guessing for to instead have the word beem automatically generated/radomized from a list of word or some other method to give the game actual challenge (instead of the person jsut knowing the word their guessing).
	//Reflection2: The only other change would be to shorten or less the amount of variable I need by either grouping them into one or through some other method that I unaware of.//
	
	
	//For printWord it will take in the information of the runGame method, more specifically if the boolean are still false or are true because the player's guess is in the word, and go down the lsit and see waht it will have to print (either printing the guessed letter of the word, as if the boolean is true, in it proper position in the word or remian blank/unchanged if the boolean/guess is false)
	//Eaxmple 1: If the word is "dino" and your/player's guess is 'n', it will go down the list and see if that letter appear in the word, finally seeing that yes it does as in Letter2 (or 3rd Letter) and  print out "Your result is __n_".
	//Example 2: If the word is "bare" and you/player has already guessed correctly 'b' and now is guessing 'e',  it will go down the list and see if that letter appear in the word, finally seeing that yes it does as in Letter3 (or 4th letter) and will add on to the previous result with "Your result is b__e".
	//Example 3: If the word is "port" adn you/player put in 'a', it will go down the list and see if that letter appear in the word, and finally see it does not appear in the world and thus leaves the result variableas is and prints out "Your result is ____".
	
	
	//For isLetterWord it should take in the variable of the word the player chose and their letter guess and see if that guess, or more specifically the letter they guessed, and give a value in accordance to where the letter is  postioned (i.e it is letter 1-4 of the word) or false with -1.
	//Eaxmple 1: If the word is "dino" and your/player's guess is 'n', it will go down the list and see if that letter appear in the word, finally seeing that yes it does as in Letter2 (or 3rd Letter) and   that it was found in position '2'(because were going from 0 to 3).
	//Example 2: If the word is "bare" and you/player has already guessed correctly 'b' and now is guessing 'e',  it will go down the list and see if that letter appear in the word, finally seeing that yes it does as in Letter3 (or 4th letter) and  state it was in position '3'.
	//Example 3: If the word is "port" adn you/player put in 'a', it will go down the list and see if that letter appear in the word, and finally see it does not appear in the world and thus leaves the result variableas is and  say it was found in -1 position.